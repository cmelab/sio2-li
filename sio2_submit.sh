#!/bin/bash -l
#SBATCH -p defq
#SBATCH -J SiO2
#SBATCH -o stdout-sio2.log
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mail-type=All
#SBATCH --mail-user=YOUREMAIL
#SBATCH -t 1:00:00
#SBATCH --gres=gpu:1

module purge
module load hoomd-blue/intel/mvapich2/2.1.5

python dftsio2.py 
