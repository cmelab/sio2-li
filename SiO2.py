# -*- coding: UTF-8 -*-
import sys
import random
import hoomd
import hoomd.md
import numpy as np


hoomd.context.initialize('--mode=gpu')

if sys.version_info[0] < 3:
    raise "Must be using Python 3"

M_Si = 28.0855 # u
M_O = 15.999 # u

M = 28.0855 # base mass unit

u_2_g = 1.6605e-24 # conversion 1 u to g
cm_2_m = 0.01
D = 3.6232e-11 # m

test = False

if test:
    N_Si = 2#36
    N_O = 4#72
else:
    N_Si = 334
    N_O = 668

assert N_Si*2 == N_O, "Need stoichiometric ratio"
N = N_Si + N_O


# Density Stuff
# 2.196 (amorphous) g·cm−3
# Haynes, William M., ed. (2011). CRC Handbook of Chemistry and Physics (92nd ed.).
# Boca Raton, FL: CRC Press. ISBN 1439855110.



mass = M_Si*N_Si*u_2_g + M_O*N_O*u_2_g # grams
rho = 2.196 # g·cm−3

L = (mass/rho)**(1/3) # cm
L = L * cm_2_m # m
L = L / D # D (diminsonless lenght units)

print("Box size (D): ", L)
print("Starting at a rho of {}, shrining to a rho of {}".format(rho, rho))



# For now we will just use a cubic cell, but we can later use a alpha or beta unitcell
snap = hoomd.data.make_snapshot(N=N, box=hoomd.data.boxdim(L=L), particle_types=['Si', 'O'])
# Set type, default is type 0 = Si, but just being explicit
snap.particles.typeid[:N_Si] = [0]*N_Si
snap.particles.typeid[-N_O:] = [1]*N_O
# Set mass
snap.particles.mass[:N_Si] = [M_Si/M]*N_Si
snap.particles.mass[-N_O:] = [M_O/M]*N_O
# Set charge
snap.particles.charge[:N_Si] = [2.4]*N_Si
snap.particles.charge[-N_O:] = [-1.2]*N_O
# Set pos (just random for now)
point_list =[]
random.seed(0)
for _ in range(N):
    x = random.uniform(-L/2.0, L/2.0)
    y = random.uniform(-L/2.0, L/2.0)
    z = random.uniform(-L/2.0, L/2.0)
    point_list.append([x, y, z])
snap.particles.position[:] = point_list

#print(snap.particles.typeid)
#print(snap.particles.mass)
#print(snap.particles.charge)

system = hoomd.init.read_snapshot(snap)
hoomd.dump.gsd(filename="start.gsd", period=None, group=hoomd.group.all(), overwrite=True)

print("Tota mass (M): ", sum(snap.particles.mass))
print("Volume (D**3): ", system.box.get_volume())
print("Density (M*D**-3): ", sum(snap.particles.mass)/system.box.get_volume())


def my_pair_potential(r, rmin, rmax, A, b, c):
    F = A*np.exp(-b*r) - (c/r**6)
    V = -A*b*np.exp(-b*r) + ((6*c)/r**7)
    return (V,F)


A_SiO = 1
b_SiO = 0.56636
c_SiO = 3.2785

A_OO = 0.07714
b_OO = 1
c_OO = 3.2785

nl = hoomd.md.nlist.cell()

table = hoomd.md.pair.table(width=int(1e4), nlist=nl)
table.pair_coeff.set('Si', 'O', func=my_pair_potential, rmin=0.7, rmax=20.0, coeff=dict(A=A_SiO, b=b_SiO, c=c_SiO))
table.pair_coeff.set('O', 'O', func=my_pair_potential, rmin=0.7, rmax=20.0, coeff=dict(A=A_OO, b=b_OO, c=c_OO))
table.pair_coeff.set('Si', 'Si', func=my_pair_potential, rmin=0.7, rmax=20.0, coeff=dict(A=0, b=0, c=0))

charged = hoomd.group.charged()
pppm = hoomd.md.charge.pppm(group=charged,  nlist=nl)
pppm.set_params(Nx=64, Ny=64, Nz=64, order=6, rcut=15.18)

hoomd.md.integrate.mode_standard(dt=0.001)
hoomd.md.integrate.nvt(group=hoomd.group.all(), kT=1, tau=5)
hoomd.analyze.log(filename="log-output.log",
                  quantities=['potential_energy', 'temperature'],
                  period=1000,
                  overwrite=True)

hoomd.dump.gsd(filename="out.gsd", period=1000, group=hoomd.group.all(), overwrite=True)
hoomd.run(1e5)
