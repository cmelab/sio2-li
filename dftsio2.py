import hoomd
import hoomd.deprecated
import hoomd.md
import numpy.random as rd
import numpy as np


# Set HOOMD to run on gpu only

hoomd.context.initialize("--mode=gpu --minimize-cpu-usage")


# Build unit cell

types = ['Si']*3 + ['O']*6
diameters = [4.2]*3 + [3.04]*6
charges = [2.4]*3 + [-1.2]*6
masses = [28.085501]*3 + [15.9994]*6
positions = [[0.0000, 0.47114, 0.83333],
            [0.52886, 0.52886, 0.50000],
            [0.47114, 0.00000, 0.16667],
            [0.14854, 0.73451, 0.04613],
            [0.58596, 0.85146, 0.37947],
            [0.26550, 0.41404, 0.71280],
            [0.41404, 0.26550, 0.28720],
            [0.85146, 0.58596, 0.62053],
            [0.73450, 0.14854, 0.95387]]


positions = np.array(positions) * np.array([4.98530, 4.98530, 5.47853]) # Go from fractional to full

a1 = np.array([4.98530, 0, 0])#/2.76
a2 = np.array([-2.49265, 4.31740, 0])#/2.76
a3 = np.array([0, 0, 5.47853])#/2.76

# Scale cell down to be deminsonless


# Create full simulation volume with 9x9x9 unit cells

uc = hoomd.lattice.unitcell(N = 9,
                            a1 = a1,
                            a2 = a2,
                            a3 = a3,
                            dimensions = 3,
                            position = positions,
                            type_name = types,
                            mass = masses,
                            charge = charges,
                            diameter = diameters)
system = hoomd.init.create_lattice(unitcell=uc, n=[8,8,8])

# Dump intial strcture 

hoomd.dump.gsd(filename="init.gsd", truncate=True, group=hoomd.group.all(), period=None)

# Define pair potental as acording to http://link.aps.org/doi/10.1103/PhysRevB.54.15808

def my_pair_potential(r, rmin, rmax, A, b, c):
    if r == 0:
        return (0, 0)
    F = A*np.exp(-b*r) - (c/r**6)
    V = -A*b*np.exp(-b*r) + ((6*c)/r**7)
    return (V,F)

# Setup function to scale particle velocites to match temperature

def init_velocity(n, temp):
    v = rd.random((n, 3))
    v -= 0.5
    meanv = np.mean(v, 0)
    meanv2 = np.mean(v ** 2, 0)
    fs = np.sqrt(temp / meanv2)
    v = (v - meanv)  # shifts the average velocity of the simulation to 0
    v *= fs  # scaling velocity to match the desired temperature
    return v


def run_md_phase(n_steps, kT_start, kT_end=None):
    
    if kT_end is None:
        kT_end = kT_start

    snap = system.take_snapshot(all=True)
    v = init_velocity(snap.particles.N, kT_start)
    snap.particles.velocity[:] = v[:]
    system.restore_snapshot(snap)

    current_step = hoomd.get_step()
    kT = hoomd.variant.linear_interp(points = [(current_step, kT_start), (current_step+n_steps, kT_end)])
    my_int.set_params(kT)
    hoomd.run(n_steps)

# Define FF coeffs

A_SiO = 18003.7572
b_SiO = 4.87318
c_SiO = 133.5381

A_OO = 1388.7730
b_OO = 2.76
c_OO = 175

# Scale FF coeffs


# Set starting temp + cooling rate
#kT_set = 300
#kT = hoomd.variant.linear_interp(points = [(0, 300), (2e4, 300), (2e4+1e6, 1), (2e4+1e6+1e6, 1
# Define neighborlist

nl = hoomd.md.nlist.cell()

# Set tell HOOMD about our FF
table = hoomd.md.pair.table(width=int(1e4), nlist=nl)
table.pair_coeff.set('Si', 'O', func=my_pair_potential, rmin=.1, rmax=10.0, coeff=dict(A=A_SiO, b=b_SiO, c=c_SiO))
table.pair_coeff.set('O', 'O', func=my_pair_potential, rmin=.1, rmax=10.0, coeff=dict(A=A_OO, b=b_OO, c=c_OO))
table.pair_coeff.set('Si', 'Si', func=my_pair_potential, rmin=0, rmax=.00000001, coeff=dict(A=0, b=0, c=0))

# Set up charges
charged = hoomd.group.charged()
pppm = hoomd.md.charge.pppm(group=charged,  nlist=nl)
pppm.set_params(Nx=64, Ny=64, Nz=64, order=6, rcut=5.5)

# Set up timestep and intergrator
#hoomd.md.integrate.mode_standard(dt=0.001)
#hoomd.md.integrate.nvt(group=hoomd.group.all(), kT=kT, tau=5)

# Set up logger
hoomd.analyze.log(filename="log-output.log",
                  quantities=['potential_energy', 'temperature'],
                  period=1000,
                  overwrite=True)

# Dump trajectory
hoomd.dump.gsd(filename="out.gsd", period=1e4, group=hoomd.group.all(), overwrite=True)
# Write out last_frame for restart purposes
hoomd.dump.gsd(filename="last_frame.gsd", period=1e4, group=hoomd.group.all(), truncate=True)


# Check to make sure density is correct
snap = system.take_snapshot(all=True)
print(system.box)
print("Tota mass (M): ", sum(snap.particles.mass))
print("Volume (D**3): ", system.box.get_volume())
print("Density (M*D**-3): ", sum(snap.particles.mass)/system.box.get_volume())

shrink_steps = 2e4
# Set distance required for amprhous density

final_L = 41.17

# Set box to resize to correct density + become cubic
lin_interp_x = hoomd.variant.linear_interp([(0, system.box.Lx), (shrink_steps, final_L)])
lin_interp_y = hoomd.variant.linear_interp([(0, system.box.Ly), (shrink_steps, final_L)])
lin_interp_z = hoomd.variant.linear_interp([(0, system.box.Lz), (shrink_steps, final_L)])

hoomd.update.box_resize(Lx = lin_interp_x,
                        Ly = lin_interp_y,
                        Lz = lin_interp_z,
                        xy = hoomd.variant.linear_interp([(0, system.box.xy), (shrink_steps, 0)]),
                        period = 1)
# Run to change box shape/size
hoomd.md.integrate.mode_standard(dt=0.001)
my_int = hoomd.md.integrate.nvt(group=hoomd.group.all(), kT=7000, tau=5)
hoomd.run(1e4)

print("done with shrink, now quenching")

run_md_phase(1e5, 7000)
run_md_phase(171000, 7000, 3000)
run_md_phase(1e5, 3000)
run_md_phase(115323, 3000, 300)
run_md_phase(1e5, 300)

# Reset velocites after box re-size 
#snap = system.take_snapshot(all=True)
#v = init_velocity(snap.particles.N, kT_set)
#snap.particles.velocity[:] = v[:]
#system.restore_snapshot(snap)
#
## Run cooling phase
#hoomd.run(5e6)

# Check to see if denisty is correct
snap = system.take_snapshot(all=True)
print(system.box)
print("Tota mass (M): ", sum(snap.particles.mass))
print("Volume (D**3): ", system.box.get_volume())
print("Density (M*D**-3): ", sum(snap.particles.mass)/system.box.get_volume())


print("Done")
